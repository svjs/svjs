# svjs

```
                 __
      _____   __/_/_____
    / ____/| / / / ____/
   _\___ \ |/ / /\___ \
  /______/___/ /______/
           \__/      

```

svjs are ES6 (ECMAScript 6) JavaScript modules, used for coding [chessmail](http://www.chessmail.eu).

The main purpose of the svjs modules is, to prevent the usage of large libraries, and to provide needed functionality with the smallest and 
cleanest amount of code possible.

All svjs modules have no external dependencies, they don't use jQuery or other frameworks. They are written with modern vanilla JavaScript in ECMAScript 6 syntax.

The svjs modules are:

- [svjs-audio](https://github.com/shaack/svjs-audio) Module for the [Web Audio API](https://developer.mozilla.org/de/docs/Web/API/Web_Audio_API). For playing audio samples in a web page.
- [svjs-svg](https://github.com/shaack/svjs-svg) Module to render SVG elements and load sprites.
- [svjs-observe](https://github.com/shaack/svjs-observe) Module to observe object properties used for reactive coding the simple way.
- [svjs-test](https://github.com/shaack/svjs-test) Very minimal module for unit testing directly in your browser.
- [svjs-app](https://github.com/shaack/svjs-app) Kind of minimal framework for ES6 JS apps.

These modules are using svjs:

- [cm-chessboard](https://github.com/shaack/cm-chessboard) The Chessboard for chessmail.de, a SVG replacement of chessboard.js
